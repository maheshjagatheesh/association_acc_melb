import { Component, OnInit, Input } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { ModalController, LoadingController, AlertController } from '@ionic/angular';
import { FixtureService, TireBasedScoring, RowingTireBasedScoring } from '../fixture.service';

export interface ScoreParms {
  labels?: string[],
  homeScores?: number[],
  awayScores?: number[],
  homeTeam?: string,
  awayTeam?: string,
  eventId?: string,
  forfiet?: string,
  washout?: string,
  forfietAway?: string,
  reportHome?: string,
  imagePath?: string,
  isFinish?: string,
  homeBestPlayer?: string,
  awayBestPlayer?: string,

  isRowing?: number,
  isTileEvent?: number,
  rowingEventId?:number[],
  rowingHomeScore?:number[],
  rowingAwayScore?:number[],
  rowingHomeTeams?: string[],
  rowingAwayTeams?: string[],
  rowingWashout?:number[],
  rowingForfeiteHome?:number[],
  rowingForfeiteAway?:number[],
  rowingReportAway?:string[],
  rowingReportHome?:string[],
  rowingIsFinish?:number[],
  rowingImage?:string[],
  rowingDocument?:string[],
  rowingHome_best_player?:string[],
  rowingAway_best_player?:string[]
}

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss' ]
})
export class ConfirmModalComponent implements OnInit {
  @Input() scoreParams: ScoreParms;
  @Input() pmodalCtrl;

  constructor(
    private modalCtrl: ModalController,
    private loadingController: LoadingController,
    private fixtureService: FixtureService,
    private alertCtrl: AlertController,
    private firebaseAnalytics: FirebaseAnalytics
  ) { }

  ngOnInit() {
  }

  finish() {
    if(this.scoreParams.isRowing == 1 || this.scoreParams.isTileEvent == 1){
      let rowingTireBasedScoring: RowingTireBasedScoring = new RowingTireBasedScoring(
        this.scoreParams.rowingEventId,
        this.scoreParams.rowingHomeScore,
        this.scoreParams.rowingAwayScore,
        this.scoreParams.rowingWashout,
        this.scoreParams.rowingForfeiteHome,
        this.scoreParams.rowingForfeiteAway,
        this.scoreParams.rowingReportAway,
        this.scoreParams.rowingReportHome,
        this.scoreParams.rowingIsFinish,
        this.scoreParams.rowingImage,
        this.scoreParams.rowingDocument,
        this.scoreParams.rowingHome_best_player,
        this.scoreParams.rowingAway_best_player
      );

      this.loadingController.create({ keyboardClose: true, message: 'Updating Score' }).then(loadingEl => {
        loadingEl.present();

        this.fixtureService.saveScore(rowingTireBasedScoring).subscribe(data => {
          loadingEl.dismiss();
          if (data) {
            this.showAlert("Scores updated successfully.");
            if (Capacitor.isNative) {
              this.firebaseAnalytics.setEnabled(true);
              this.firebaseAnalytics.logEvent("Score_saved", {event_id: rowingTireBasedScoring.eventId[0]})
                .then((res: any) => console.log(res)).catch((error: any) => console.error(error));
            }
            this.onSuccess();
          }
        })
      })
    } else{
      let tireBasedScoring: TireBasedScoring = new TireBasedScoring(
        this.scoreParams.awayScores,
        this.scoreParams.homeScores,
        this.scoreParams.eventId,
        this.scoreParams.forfiet.toString(),
        this.scoreParams.washout.toString(),
        this.scoreParams.forfietAway.toString(),
        this.scoreParams.reportHome,
        this.scoreParams.imagePath,
        this.scoreParams.isFinish,
        this.scoreParams.homeBestPlayer,
        this.scoreParams.awayBestPlayer
      );
  
  
      this.loadingController.create({ keyboardClose: true, message: 'Updating Score' }).then(loadingEl => {
        loadingEl.present();
  
        this.fixtureService.updateScoreTireBased(tireBasedScoring).subscribe(data => {
            loadingEl.dismiss();
            if (data) {
              this.showAlert("Scores updated successfully.");
              if (Capacitor.isNative) {
                this.firebaseAnalytics.setEnabled(true);
                this.firebaseAnalytics.logEvent("Score_saved", {event_id: tireBasedScoring.event_id})
                  .then((res: any) => console.log(res)).catch((error: any) => console.error(error));
              }
              this.onSuccess();
            }
          })
      })
    }
  }
  onCancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }

  onSuccess() {
    this.modalCtrl.dismiss(true, 'success');
  }

  showAlert(message: string) {
    return this.alertCtrl
      .create({
        header: 'Success',
        message: message,
        buttons: [
          {
            text: 'Okay',
          }
        ]
      })
      .then(alertEl => alertEl.present());
  }

}
