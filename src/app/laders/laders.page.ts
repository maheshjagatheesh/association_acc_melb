import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { LaderService } from './lader.service';
import { environment } from '../../environments/environment';
import { DeviceService } from '../shared/device/device.service';
import { CompetitionFilterService } from '../shared/components/competition-filter/competition-filter.service';
import { Subscription } from 'rxjs';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { Capacitor } from '@capacitor/core';

@Component({
  selector: 'app-laders',
  templateUrl: './laders.page.html',
  styleUrls: ['./laders.page.scss'],
})
export class LadersPage implements OnInit {
  isLoading = false;
  ladderGames: [] = [];
  favouriteTeams: [] = [];
  imgBaseUrl = environment.logoImgPath;
  competitionSubscription: Subscription;
  constructor(
    private menuCntrl: MenuController,
    private ladderService: LaderService,
    private deviceService: DeviceService,
    private competitionFilterService: CompetitionFilterService,
    private firebaseAnalytics: FirebaseAnalytics
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    // if (Capacitor.isNative) {
    //   this.firebaseAnalytics.setEnabled(true);
    //   this.firebaseAnalytics.setCurrentScreen('Ladder')
    //     .then((res: any) => console.log(res)).catch((error: any) => console.error(error));
    // }

    // this.isLoading = true;
    console.log("----ION---");
    this.loadLadderGames();
    this.menuCntrl.enable(false);

    this.competitionSubscription = this.competitionFilterService.subAssociationFilterSelected.subscribe(data => {
      if (data) {
        this.loadLadderGames();
      }
    })

  }

  ionViewDidEnter(){
    if (Capacitor.isNative) {
      this.firebaseAnalytics.setEnabled(true);
      this.firebaseAnalytics.setCurrentScreen('Ladder')
        .then((res: any) => console.log(res)).catch((error: any) => console.error(error));
    }
  }

  toggleFilter() {
    this.menuCntrl.toggle()
  }

  loadLadderGames() {
    this.isLoading = true;
    this.deviceService.getDeviceId().then(data => {

      this.ladderService.getLadderGame(data.uuid.toString(), data.model.toString()).subscribe(data => {
        this.isLoading = false;
        this.ladderGames = data.GETGAME;
        this.favouriteTeams = data.FAVOURITETEAM;
        console.log(data);
      })
    })

  }

  ionViewDidLeave() {
    this.competitionSubscription.unsubscribe(); // unsubscribe
  }

}
