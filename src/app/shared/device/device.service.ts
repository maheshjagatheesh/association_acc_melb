import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';

const { Device } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  constructor() { }

  async getDeviceId(){
    const info = await Device.getInfo();
    console.log(info);
    return info;
  }

  getFCMToken() {

    return from(Plugins.Storage.get({ key: 'FCMToken' })).pipe(
      map(storedData => {
        if (!storedData || !storedData.value) {
          return null;
        }
        const parsedData = JSON.parse(storedData.value);
        // console.log("parsedData" + JSON.stringify(parsedData));

        if (parsedData) {
          // const user = new User(
          //   parsedData._season_id,
          //   parsedData._client_id,
          //   parsedData._adminLevel,
          //   parsedData._person_id
          // );
          return {};

        } else {
          return null;
        }
      })
    );

  }
}
