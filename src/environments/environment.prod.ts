export const environment = {
  production: true,
  baseURL: "http://api.gojaro.com/rest/jaro/",
  googleMapApiKey: "AIzaSyBHAFvD1hwJTWvFqjKzqIxzydq5D1Ksf4I",
  logoImgPath: "http://database.gojaro.com/logos/",
  masterClientId: "95",
  masterPersonId: "31967",
  liveScoringEnable: false,
  showMoreTab: false,
  appName: 'com.association.accwaapp',
  appVersion: '1.1.0'
};
