import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.association.accmelb',
  appName: 'Association_ACC_MELB',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
